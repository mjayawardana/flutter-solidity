pragma solidity ^0.5.9;
  
contract HelloWorld {
  string yourName ;

constructor() public {
        yourName = "Unknown" ;
}

function getName() public view returns (string memory){
       return yourName  ;
}

function setName(string memory nm) public{
        yourName = nm ;
}
}